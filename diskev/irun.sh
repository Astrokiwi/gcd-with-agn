#!/bin/bash
#PBS -l nodes=4:ppn=16
#PBS -j oe
#PBS -o gcd_out
#PBS -l walltime=0:30:00
#PBS -N agn_test
ulimit -a
cd $PBS_O_WORKDIR
mpirun -np 64 ./gcdp
