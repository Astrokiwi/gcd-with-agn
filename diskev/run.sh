#!/bin/bash
#PBS -l nodes=9:ppn=8
#PBS -j oe
#PBS -o gcd_out
#PBS -l walltime=43:00:00
#PBS -N gcdp11xx
#PBS -A dhg-391-ad
ulimit -a
cd $PBS_O_WORKDIR
mpiexec -n 72 ./gcdp
