#!/bin/bash
#PBS -l nodes=4:ppn=8
#PBS -j oe
#PBS -o gcd_out_1
#PBS -l walltime=8:00:00
#PBS -N agn_test
#PBS -A dhg-391-ac
ulimit -a
cd $PBS_O_WORKDIR
mpiexec -n 32 ./gcdpc 4
