#!/bin/bash
#PBS -l procs=12
#PBS -j oe
#PBS -o gcd_out
#PBS -l pmem=1700m
#PBS -l walltime=0:30:00
#PBS -N agn_test
#PBS -A dhg-391-ac
ulimit -a
cd $PBS_O_WORKDIR
mpirun -np 12 ./gcdp
