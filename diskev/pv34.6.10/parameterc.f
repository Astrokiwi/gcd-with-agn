C ------physcial constants and unit conversions
C     extracted from a code by Jeremy Kepner (1997)
C ----- Declare array dimension paramters.
        INTEGER 
     &    N_T, N_nu, N_pts_bound, N_nu_base,N_nu_bound
        PARAMETER(N_pts_bound = 7
     &   ,N_nu_base   = 200
     &   ,N_nu_bound  = 12
     &   ,N_T    = 1000
     &   ,N_nu        = N_nu_base + N_nu_bound*N_pts_bound
     &      )

C ----- Declare some physical constants (cgs).
        DOUBLE PRECISION pc_to_cm,Kpc_to_cm,Mpc_to_cm,yr_to_sec
     $       ,kms_to_cms,
c         M_sun,L_sun,lh,H0,t_hubble0,Omega_b,big_G,boltz,gamma,mh,
     &   eV_to_K,eV_to_erg,eV_to_Hz,tiny,huge,pi
c         ,rho_c0
        PARAMETER(
     &    pc_to_cm  = 3.086e+18,
     &    Kpc_to_cm = 3.086e+21,
     &    Mpc_to_cm = 3.086e+24,
     &    yr_to_sec = 3.16676e7,
     &    kms_to_cms= 1.e5,
c     &    M_sun     = 2.e33,
c     &    L_sun     = 4.e33,
c     &    lh        = 0.5d0,
c     &    H0        = lh*3.24e-18,
c     &    t_hubble0 = 2.0d0/(3.0d0*H0),
c     &    Omega_b   = 0.1,
c     &    big_G     = 6.67e-8,
c     &    boltz     = 1.38e-16,
c     &    gamma     = 1.666,
c     &    mh        = 1.67e-24,
     &    eV_to_K   = 11606.0,
     &    eV_to_erg = 1.60184e-12,
     &    eV_to_Hz  = 2.41838e14,
     &    tiny      = 0.0d0,
     &    huge      = 1.e20,
     &    pi        = 3.14159265
c     &    rho_c0    = (3. * (H0)**2) / (8. * pi * big_G)
     &  )

C ----- Set various cutoff values in eV.
        DOUBLE PRECISION e24,e25,e26,e27,e28a,e28b,e28c,e29a,e29b,e29c
     $       ,e30a,e30b 
        PARAMETER(
     &    e24  = 13.6d0
     &   ,e25  = 54.4d0
     &   ,e26  = 24.6d0
     &   ,e27  = 0.755d0
     &   ,e28a = 2.65d0
     &   ,e28b = 11.27d0
     &   ,e28c = 21.0d0
     &   ,e29a = 15.42d0
     &   ,e29b = 16.5d0
     &   ,e29c = 17.7d0
     &   ,e30a = 30.0d0
     &   ,e30b = 70.0d0
     &  )

C ----- Declare some control parameters.
c        LOGICAL H_flag,He_flag,H2_flag,species_flag
c     &         ,opt_thin_flag,rad_flag
c     &         ,energy_flag
c     &         ,H2_shield_flag
c        PARAMETER(
c     &    H_flag               = .TRUE.
c     &   ,He_flag              = .TRUE.
c     &   ,H2_flag              = .TRUE.
c     &   ,species_flag         = .TRUE.
cc     &   ,opt_thin_flag        = .FALSE.
c     &   ,opt_thin_flag        = .TRUE.
c     &   ,rad_flag             = .TRUE.
c     &   ,energy_flag          = .TRUE.
c     &   ,H2_shield_flag       = .TRUE.
c     &   )


C ----- Declare derived flags.
c        LOGICAL
c     &    k1_flag,k2_flag,k3_flag,k4_flag,k5_flag,k6_flag
c     &   ,k7_flag,k8_flag,k9_flag,k10_flag,k11_flag,k12_flag
c     &   ,k13_flag,k14_flag,k15_flag,k16_flag,k17_flag,k18_flag
c     &   ,k19_flag,k20_flag,k21_flag
c     &   ,k24_flag,k25_flag,k26_flag,k27_flag
c     &   ,k28_flag,k29_flag,k30_flag,k31_flag

C  Abel, Annions, Zhang & Norman astro-ph/960840 coef re-mapping is on left.
C ------- 1:	HI    + e   -> HII   + 2e
C ------- 2:	HII   + e   -> H     + p
C ------- 3:	HeI   + e   -> HeII  + 2e
C ------- 4:	HeII  + e   -> HeI   + p
C ------- 5:	HeII  + e   -> HeIII + 2e
C ------- 6:	HeIII + e   -> HeII  + p
C ------- 7:	HI    + e   -> HM    + p
C ------- 8:	HM    + HI  -> H2I*  + e
C ------- 9:    HI    + HII -> H2II  + p
C ------- 10:	H2II  + HI  -> H2I*  + HII
C ---13-- 11:	H2I   + H   -> 3H
C ---11-- 12:	H2I   + HII -> H2II  + H
C ---NA-- 13:	H2I   + e   -> HI    + HM
C ---12-- 14:	H2I   + e   -> 2HI   + e
C ---NA-- 15:	H2I   + H2I -> H2I   + 2HI
C ---14-- 16:	HM    + e   -> HI    + 2e
C ---15-- 17:	HM    + HI  -> 2H    + e
C ---16-- 18:	HM    + HII -> 2HI
C ---17-- 19:	HM    + HII -> H2II  + e
C ---18-- 20:	H2II  + e   -> 2HI
C ---19-- 21:	H2II  + HM  -> HI    + H2I
C ---20-- 24:	HI    + p   -> HII   + e
C ---22-- 25:	HeII  + p   -> HeIII + e
C ---21-- 26:	HeI   + p   -> HeII  + e
C ---23-- 27:	HM    + p   -> HI    + e
C ---25-- 28:	H2II  + p   -> HI    + HII
C ---24-- 29:	H2I   + p   -> H2II  + e
C ---26-- 30:	H2II  + p   -> 2HII  + e
C -27+28- 31:	H2I   + p   -> 2HI

c        PARAMETER(
c     &    k1_flag = H_flag
c     &   ,k2_flag = H_flag
c     &   ,k3_flag = He_flag
c     &   ,k4_flag = He_flag
c     &   ,k5_flag = He_flag
c     &   ,k6_flag = He_flag
c     &   ,k7_flag = H_flag
c     &   ,k8_flag = H_flag .AND. H2_flag
c     &   ,k9_flag = H_flag .AND. H2_flag
c     &   ,k10_flag = H_flag .AND. H2_flag
c     &   ,k11_flag = H_flag .AND. H2_flag
c     &   ,k12_flag = H_flag .AND. H2_flag
c     &   ,k13_flag = H_flag .AND. H2_flag
c     &   ,k14_flag = H_flag .AND. H2_flag
c     &   ,k15_flag = H_flag .AND. H2_flag
c     &   ,k16_flag = H_flag .AND. H2_flag
c     &   ,k17_flag = H_flag .AND. H2_flag
c     &   ,k18_flag = H_flag .AND. H2_flag
c     &   ,k19_flag = H_flag .AND. H2_flag
c     &   ,k20_flag = H_flag .AND. H2_flag
c     &   ,k21_flag = H_flag .AND. H2_flag
c     &   ,k24_flag = H_flag .AND. rad_flag
c     &   ,k25_flag = He_flag .AND. rad_flag
c     &   ,k26_flag = He_flag .AND. rad_flag
c     &   ,k27_flag = H_flag .AND. H2_flag .AND. rad_flag
c     &   ,k28_flag = H_flag .AND. H2_flag .AND. rad_flag
c     &   ,k29_flag = H_flag .AND. H2_flag .AND. rad_flag
c     &   ,k30_flag = H_flag .AND. H2_flag .AND. rad_flag
c     &   ,k31_flag = H_flag .AND. H2_flag .AND. rad_flag
c     &  )

C -------------- VARIABLES -----------------

C ----- Units.
c        DOUBLE PRECISION base_len,base_time,base_vol,base_den
c     &      ,base_nden,kunit,base_J
        DOUBLE PRECISION base_time,base_nden,base_J

C ----- Grid spacings.
        DOUBLE PRECISION dlog_T,dlog_nu

C ----- Some grid bounds.
        DOUBLE PRECISION T_start,T_end, T(N_T)
     $       ,nu(N_nu), nu_start, nu_end

        DOUBLE PRECISION
C     species fractions 
     $     H, He,  HI,  HII,  HeI
     $     ,HeII,HeIII,   HM
     $     , H2I, H2II, elec

C ----- Radiation field 
c        DOUBLE PRECISION J0_nu(N_nu),J_nu(N_nu)  
        DOUBLE PRECISION J_nu(N_nu)
c        double precision J_21,alphaJ

C     dump arrays
c        DOUBLE PRECISION
C     species
c     $       fH(N_data_dumps), fHe(N_data_dumps),  fHI(N_data_dumps), 
c     $       fHII(N_data_dumps),  fHeI(N_data_dumps),fHeII(N_data_dumps)
c     $       ,fHeIII(N_data_dumps),   fHM(N_data_dumps),
c     $       fH2I(N_data_dumps), fH2II(N_data_dumps),felec(N_data_dumps)
C redshift, gas temperature,  density and total number density
c     $       ,fz(N_data_dumps), fT_gas(N_data_dumps),
c     $       frho_gas(N_data_dumps) , fnden(N_data_dumps),
c     $       flength(N_data_dumps) 
        
C ----- Coefficient arrays.
      DOUBLE PRECISION k1a(N_T),k2a(N_T),k3a(N_T),k4a(N_T),k5a(N_T)
     $       ,k6a(N_T),k7a(N_T),k8a(N_T),k9a(N_T),k10a(N_T),k11a(N_T)
     $       ,k12a(N_T),k13a(N_T),k14a(N_T),k15a(N_T),k16a(N_T),k17a(N_T
     $       ),k18a(N_T),k19a(N_T),k20a(N_T),k21a(N_T)

        DOUBLE PRECISION k24,k25,k26,k27
     &      ,k28,k29,k30,k31

        DOUBLE PRECISION ceHIa(N_T),ceHeIa(N_T),ceHeIIa(N_T)
     &      ,ciHIa(N_T),ciHeIa(N_T),ciHeISa(N_T),ciHeIIa(N_T)
     &      ,reHIIa(N_T),reHeII1a(N_T),reHeII2a(N_T),reHeIIIa(N_T)
     &      ,brema(N_T),
     &       ldla(N_T),hdlra(N_T),hdlva(N_T)
c     &       hyd01ka(N_T),h2k01a(N_T),vibha(N_T)
c     &      ,rotha(N_T),rotla(N_T)

        DOUBLE PRECISION sigma24(N_nu),sigma25(N_nu),sigma26(N_nu)
     $       ,sigma27(N_nu),sigma28(N_nu),sigma29(N_nu),sigma30(N_nu)
     $       ,sigma31(N_nu)
        
        DOUBLE PRECISION piHI,piHeI,piHeII

C ----- Energy array.s
        DOUBLE PRECISION 
     &       edot_ceHI,edot_ceHeI,edot_ceHeII
     &       ,edot_ciHI,edot_ciHeI,edot_ciHeII
     $       ,edot_ciHeIS,edot_reHI,edot_reHeII1
     $       ,edot_reHeII2,edot_reHeIII,edot_comp
     $       ,edot_brem,edot_H2,edot_radHI
     $       ,edot_radHeI,edot_radHeII


        COMMON/Mainb/
c     &       base_len,base_time,base_vol,base_den
     &       base_time,base_nden,base_J
     &       ,dlog_T,T_start,T_end,T,dlog_nu,nu,nu_start,nu_end
     &       ,k1a,k2a,k3a,k4a,k5a,k6a
     &       ,k7a,k8a,k9a,k10a,k11a,k12a
     &       ,k13a,k14a,k15a,k16a,k17a,k18a
     &       ,k19a,k20a,k21a
     &       ,k24,k25,k26,k27
     &       ,k28,k29,k30,k31
     &       ,ceHIa,ceHeIa,ceHeIIa
     &       ,ciHIa,ciHeIa,ciHeISa,ciHeIIa
     &       ,reHIIa,reHeII1a,reHeII2a,reHeIIIa
        COMMON/Mainb/
c     &       brema,hyd01ka,h2k01a,vibha,rotha,rotla
     &       brema,ldla,hdlra,hdlva
     &       ,sigma24,sigma25,sigma26,sigma27
     &       ,sigma28,sigma29,sigma30,sigma31
     &       ,piHI,piHeI,piHeII
     &       ,edot_ceHI,edot_ceHeI,edot_ceHeII
     &       ,edot_ciHI,edot_ciHeI,edot_ciHeII,edot_ciHeIS
     &       ,edot_reHI,edot_reHeII1,edot_reHeII2,edot_reHeIII
     &       ,edot_comp,edot_brem,edot_H2
     &       ,edot_radHI,edot_radHeI,edot_radHeII
     $       ,  H, He,  HI,  HII,  HeI
     $       ,HeII,HeIII,   HM
     $       , H2I, H2II, elec
c     $       , fH, fHe, fHI, fHII, fHeI,fHeII
c     $       ,fHeIII, fHM, fH2I, fH2II,felec
c     $       ,fz, fT_gas, frho_gas, fnden, flength
c     $       ,J0_nu,J_nu,J_21,alphaJ
     $       ,J_nu

