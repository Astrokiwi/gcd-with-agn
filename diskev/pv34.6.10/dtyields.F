#include "gcdp.def"
c ******************************************
c   dtyields.f for GCD+ pv32.1 
c  08 Feb. 2010  written by D. Kawata
c ******************************************
c ****************************************************
c  return total ejected mass until TM_tot+TM_dt
c ****************************************************
c70*******************************************************************

      subroutine dtyields(ng,ns)
#if defined(SF_EFD) || defined(SF_ZFD)
      include 'common.for'
      integer ng,ns
      integer i,j,pn,isp
      integer iz1,it1
      double precision zg(0:MNB-1),age(0:MNB-1),ms0p
c *** for interpolation ***
      double precision wz1,wz2,wt1,wt2,dz,dt,dtsw,agesw
c *** for initial value for mass group ***
      double precision tsp1,tsp2,tspiz,tspez,dtsp
c      double precision tsfd,rhoc
c * for feedback  common * ! I don't think these are ever actually used
      double precision meji_sp,nsni_sp,mzHei_sp,mzCi_sp
     &  ,mzNi_sp,mzOi_sp,mzNei_sp,mzMgi_sp,mzSi_sp,mzFei_sp,tmzZ_sp
     &  ,nsw_sp
c * Total *
      double precision tmej_sn,tnsn_sn,tmzHe_sn,tmzC_sn,tmzN_sn,
     &  tmzO_sn,tmzNe_sn,tmzMg_sn,tmzSi_sn,tmzFe_sn,tmzZ_sn
     &  ,nsw_sn
      common /TFEEDB/tmej_sn(0:MNB-1),tnsn_sn(0:MNB-1),
     & tmzHe_sn(0:MNB-1),tmzC_sn(0:MNB-1),tmzN_sn(0:MNB-1),
     & tmzO_sn(0:MNB-1),tmzNe_sn(0:MNB-1),tmzMg_sn(0:MNB-1),
     & tmzSi_sn(0:MNB-1),tmzFe_sn(0:MNB-1),tmzZ_sn(0:MNB-1)
     & ,nsw_sn(0:MNB-1)

      double precision gkmm,tenkk
      gkmm=(GAM-1.0d0)/((KCGS/(MP*MYU))/K_MU)

c *** star formation delay time ***
c      rhoc=(MP/DU)*NSTH/XHSOL
c *** time to make stars SI_nsp x mp
c      tsfd=dble(SI_nsp)*TMUGYR*dsqrt(3.0d0*M_PI/(16.0d0*G*rhoc))
c     &  /CSEFF

c *** Metallicity of Age for each star and gas ***
      do i = 0,ng+ns-1
        pn = list_ap(i)
#ifdef METAL
c *** assuming m_p never change, use original Z ***
        zg(pn) = mzZ0_p(pn)/(m_p(pn)*MUSM)
#else
        zg(pn) = 0.0d0
#endif
c since pv31.6
c        age(pn) = (TMsf_t+TMsf_dt-ts_p(pn))*TMUGYR
c since pv32.0
        age(pn) = (TM_tot+TM_dt-ts_p(pn))*TMUGYR
      enddo

      dz=(dlog10(z_ytb(NYTZ+1))-dlog10(z_ytb(1)))/dble(NYTZ)
      dt=(dlog10(t_ytb(NYTT+1))-dlog10(t_ytb(1)))/dble(NYTT)
c *** Get the yields ***
      do i = 0,ng+ns-1
        pn = list_ap(i)
        tmej_sn(pn)=0.0d0
        tnsn_sn(pn)=0.0d0
c *** Unit Solar Mass ***
        tmzHe_sn(pn)=0.0d0
        tmzC_sn(pn)=0.0d0
        tmzN_sn(pn)=0.0d0
        tmzO_sn(pn)=0.0d0
        tmzNe_sn(pn)=0.0d0
        tmzMg_sn(pn)=0.0d0
        tmzSi_sn(pn)=0.0d0
        tmzFe_sn(pn)=0.0d0
        tmzZ_sn(pn)=0.0d0
c *** number of stars with stellar wind ***
        nsw_sn(pn)=0.0d0
        if(flagfd_p(pn).ne.0 ) then
          if(flagfd_p(pn).gt.0) then
            isp=flagfd_p(pn)-1
          else
            isp=-flagfd_p(pn)-1
          endif
c *** for metallicity weight ***
          if(zg(pn).ge.z_ytb(1)) then
            iz1=int((dlog10(zg(pn))-dlog10(z_ytb(1)))/dz)+1
          else 
            iz1=0
          endif
c *** only if the particles are qualified as feedback gas 
c *** in both metallicity tables of iz1 and iz1
          if(isp.lt.nsp_ytb(iz1)-1
     &      .and.isp.lt.nsp_ytb(iz1+1)-1) then
c *** weight for metallicity table ***
            wz2=(zg(pn)-z_ytb(iz1))/(z_ytb(iz1+1)-z_ytb(iz1))
            wz1=1.0d0-wz2
c *** calculate starting time of the group ***
            if(isp.lt.SI_snii) then
c *** if mass group includes SNe II, all treat as a SNe II group
              tspiz=0.0d0
#ifdef DELAYSNII
              tspiz=tspiz+5.d6/TMUYR ! Delay by 5 Myr
#endif
c *** calculate starting time of the group ***
              tsp1=tspi_ytb(SI_snii,iz1)
              tsp2=tspi_ytb(SI_snii,iz1+1)
              tspez=tsp1*wz1+tsp2*wz2
#ifdef DELAYSNII
              tspez=tspez+tspiz
#endif
            else
              tsp1=tspi_ytb(isp,iz1)
              tsp2=tspi_ytb(isp,iz1+1)
c *** starting time of the mass group ***
              tspiz=tsp1*wz1+tsp2*wz2
c *** end time of group mass ***
c *** calculate starting time of the group ***
              tsp1=tspi_ytb(isp+1,iz1)
              tsp2=tspi_ytb(isp+1,iz1+1)
              tspez=tsp1*wz1+tsp2*wz2
            endif
c *** feedback duration of the mass group ***
            dtsp=tspez-tspiz

#ifdef DELAYSNII
            if ( age(pn)<=tspiz .and. isp<SI_snii ) then
                ! Hard-code the temperature!
                tenkk = 1.d4*(GAM-1.d0)/
     &                (rho_p(pn)**(GAM-1.d0)*myu_p(pn)*gkmm)
                as_p(pn) = max(as_p(pn),tenkk)
                p_p(pn) = rho_p(pn)**GAM*as_p(pn)
                u_p(pn) = p_p(pn)/((GAM-1.0d0)*rho_p(pn))
            endif
#endif

            if(age(pn).gt.tspiz) then
              if(flagfd_p(pn).gt.0) then
                flagrfd_p(pn)=1    
              endif
              if(flagfd_p(pn).gt.0
     &          .and.tspiz.gt.age(pn)-TM_dt*TMUGYR) then
c *** set initial values, including the case of isp<SI_snii ***
                if(tspiz.ge.t_ytb(1)) then 
                  it1=int((dlog10(tspiz)-dlog10(t_ytb(1)))/dt)+1
                else
                  it1=0
                endif
                wt2=(tspiz-t_ytb(it1))/(t_ytb(it1+1)-t_ytb(it1))
                wt1=1.0d0-wt2
              else
                if(age(pn).gt.tspez) then
                  age(pn)=tspez
c *** flagrfd_p will be used in starfd ***
                  flagrfd_p(pn)=-1
                endif
c *** feedback gas particles ***
c *** search time ***
                if(age(pn).ge.t_ytb(1)) then 
                  it1=int((dlog10(age(pn))-dlog10(t_ytb(1)))/dt)+1
                else
                  it1=0
                endif
                wt2=(age(pn)-t_ytb(it1))/(t_ytb(it1+1)-t_ytb(it1))
                wt1=1.0d0-wt2
              endif
c *** each particles are 1/SI_nsp part ***
              if(isp.lt.SI_snii) then
                ms0p=m_p(pn)*dble(SI_nsp)/dble(SI_snii)
              else
                ms0p=m_p(pn)*dble(SI_nsp)
              endif  
c *** calculate total yield up the age ***
              tmej_sn(pn)=(wt1*wz1*mej_ytb(it1,iz1)
     &   +wt1*wz2*mej_ytb(it1,iz1+1)+wt2*wz1*mej_ytb(it1+1,iz1)
     &   +wt2*wz2*mej_ytb(it1+1,iz1+1))*ms0p
              tnsn_sn(pn)=(wt1*wz1*nsn_ytb(it1,iz1)
     &   +wt1*wz2*nsn_ytb(it1,iz1+1)+wt2*wz1*nsn_ytb(it1+1,iz1)
     &   +wt2*wz2*nsn_ytb(it1+1,iz1+1))*(ms0p*MUSM)
c
c              if(flagfd_p(pn).lt.0) then
c              write(6,*) ' id,fd=',id_p(pn),flagfd_p(pn)
c     &         ,it1,iz1,wt1,wt2,wz1,wz2,SI_nsp,ms0p
c     &         ,tnsn_sn(pn),nsn_ytb(it1,iz1),age(pn)
c     &         ,ts_p(pn),(TM_tot+TM_dt-ts_p(pn))*TMUGYR
c              endif
c
c *** Unit Solar Mass ***
              tmzHe_sn(pn)=(wt1*wz1*mzHe_ytb(it1,iz1)
     &   +wt1*wz2*mzHe_ytb(it1,iz1+1)+wt2*wz1*mzHe_ytb(it1+1,iz1)
     &   +wt2*wz2*mzHe_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzC_sn(pn)=(wt1*wz1*mzC_ytb(it1,iz1)
     &   +wt1*wz2*mzC_ytb(it1,iz1+1)+wt2*wz1*mzC_ytb(it1+1,iz1)
     &   +wt2*wz2*mzC_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzN_sn(pn)=(wt1*wz1*mzN_ytb(it1,iz1)
     &   +wt1*wz2*mzN_ytb(it1,iz1+1)+wt2*wz1*mzN_ytb(it1+1,iz1)
     &   +wt2*wz2*mzN_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzO_sn(pn)=(wt1*wz1*mzO_ytb(it1,iz1)
     &   +wt1*wz2*mzO_ytb(it1,iz1+1)+wt2*wz1*mzO_ytb(it1+1,iz1)
     &   +wt2*wz2*mzO_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzNe_sn(pn)=(wt1*wz1*mzNe_ytb(it1,iz1)
     &   +wt1*wz2*mzNe_ytb(it1,iz1+1)+wt2*wz1*mzNe_ytb(it1+1,iz1)
     &   +wt2*wz2*mzNe_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzMg_sn(pn)=(wt1*wz1*mzMg_ytb(it1,iz1)
     &   +wt1*wz2*mzMg_ytb(it1,iz1+1)+wt2*wz1*mzMg_ytb(it1+1,iz1)
     &   +wt2*wz2*mzMg_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzSi_sn(pn)=(wt1*wz1*mzSi_ytb(it1,iz1)
     &   +wt1*wz2*mzSi_ytb(it1,iz1+1)+wt2*wz1*mzSi_ytb(it1+1,iz1)
     &   +wt2*wz2*mzSi_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzFe_sn(pn)=(wt1*wz1*mzFe_ytb(it1,iz1)
     &   +wt1*wz2*mzFe_ytb(it1,iz1+1)+wt2*wz1*mzFe_ytb(it1+1,iz1)
     &   +wt2*wz2*mzFe_ytb(it1+1,iz1+1))*(ms0p*MUSM)
              tmzZ_sn(pn)=(wt1*wz1*mzZ_ytb(it1,iz1)
     &   +wt1*wz2*mzZ_ytb(it1,iz1+1)+wt2*wz1*mzZ_ytb(it1+1,iz1)
     &   +wt2*wz2*mzZ_ytb(it1+1,iz1+1))*(ms0p*MUSM)
c *** number of stars with stellar wind x dt ***
c because age is modified, cannot calculate mean one.
c use the time at age
c              dtsw=age(pn)/TMUGYR+ts_p(pn)-TM_tot
c              agesw=age(pn)-0.5d0*dtsw*TMUGYR
c              if(agesw.ge.t_ytb(1)) then 
c                it1=int((dlog10(agesw)-dlog10(t_ytb(1)))/dt)+1
c              else
c                it1=0
c              endif
c              wt2=(agesw-t_ytb(it1))/(t_ytb(it1+1)-t_ytb(it1))
c              wt1=1.0d0-wt2
              dtsw=TM_dt
              nsw_sn(pn)=(wt1*wz1*nsw_ytb(it1,iz1)
     &   +wt1*wz2*nsw_ytb(it1,iz1+1)+wt2*wz1*nsw_ytb(it1+1,iz1)
     &   +wt2*wz2*nsw_ytb(it1+1,iz1+1))*(ms0p*MUSM)*dtsw
            endif
          endif
        endif
      enddo
#endif
      return
      end



