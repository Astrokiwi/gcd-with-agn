#include "gcdp.def"

c debuggering stuff


      subroutine checklist(checkstr,ng,ns,np)
       include './common.for'
       
       integer :: tng,tns ! ng,ns in test counts
       
       integer :: pn,ii
       
       integer :: ng,ns,np ! number of particles of each component
       
       character*32 :: checkstr ! label to show where we died
       
       logical, dimension(0:np-1) :: inlist_ap
       
       tng = 0
       tns = 0
       
       inlist_ap = .false.
       
       do ii=0,np-1
        pn = list_ap(ii)
        if ( pn>=np .or. pn<0 ) then
         write(*,"(I2.2,A,A,4I7)") myrank,trim(checkstr)," bad pn in gas",
     &         pn,np,ng,ns
         stop
        endif
        
        if ( flagc_p(pn)>=1 ) then
         exit ! found a star
        else
         tng = tng + 1
         inlist_ap(pn) = .true.
        endif
       end do
       
       if ( tng/=ng ) then
        write(*,"(I2.2,A,A,4I7)") myrank,trim(checkstr)," list_ap tng!=ng ",
     &        tng,ng,ns,np
        stop
       endif
       
       do ii=tng,np-1
        pn = list_ap(ii)
        if ( pn>=np .or. pn<0 ) then
        write(*,"(I2.2,A,A,4I7)") myrank,trim(checkstr)," bad pn in star",
     &        pn,np,ng,ns
         stop
        endif
        
        if ( flagc_p(pn)<=0 ) then
        write(*,"(I2.2,A,A,4I7)")
     &        myrank,trim(checkstr)," found gas in star list_ap",pn,np,ng,ns
         stop
        else
         tns = tns + 1
         inlist_ap(pn) = .true.
        endif
       end do

       if ( tns/=ns ) then
        write(*,"(I2.2,A,A,5I7)") myrank,trim(checkstr)," list_ap tns!=ns ",
     &        tng,ng,tns,ns,np
        stop
       endif
       
       do ii=0,np-1
        if ( .not.inlist_ap(ii) ) then
        write(*,"(I2.2,A,A,4I7)")
     &        myrank,trim(checkstr)," not found in list_ap ",ii,ng,ns,np
         stop
        endif
       end do
      
      end subroutine checklist
      