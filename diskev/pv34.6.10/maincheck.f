c /*************************************
c    main.f for GCD+ pver.28
c  24 Feb., 2008    produced by D.KAWATA
c *************************************/
c70*******************************************************************

      include "common.f"

c /* Total Number of Particles, Active */	  
      integer npt,np,pn
c /* Total Number of Gas Particles, Active */	  
      integer ng(0:NCPU-1),nag
c /* Total Number of DM Particles, Active */	  
      integer ndmt,ndm,nadm
c /* Number of Star Particles, Active */	  
      integer ns,nas
c /* Number of BH particle */	  
      integer nbh
c /* Number of system steps */	  
      integer step
c /* Local step in system step */	  
      integer lstep
c /* Total Time Steps */	  
      integer totstep
c /* Flag for Kin.eq , Ene.eq */	  
      integer flagv,flagu
      integer i,j,ip
c /* Specific Heat Ratio */	  
      double precision gam
c /* flag for output */
      integer flagend
      integer flagcont
      integer flagt
c /* for continue */	
      integer tstep
      integer idc_p(0:MN-1,0:NCPU-1)
c *** for test ***
      character fileo*60
c /*****   parameter values   *****/
c /* for time */
      double precision itime,ftime,ntime
      double precision JOBTIME,MARGIN
      parameter (JOBTIME=14400.0d0)
      parameter (MARGIN=300.0d0)
      integer OUTSTEP
      parameter (OUTSTEP=100)

      do totstep=1,20
        do ip=0,1
c *** test output ***
          write(fileo,'(a5,i3.3,i3.3)') 'main2',ip,totstep
          open(50,file=fileo,status='unknown')
         do i=0,MN-1
          read(50,162,end=70) xc_p(i),yc_p(i),zc_p(i)
     &  ,vnx_p(i),vny_p(i),vnz_p(i),m_p(i),rho_p(i),u_p(i)
     &  ,mzHe_p(i),mzC_p(i),mzN_p(i),mzO_p(i),mzNe_p(i),mzMg_p(i)
     &  ,mzSi_p(i),mzFe_p(i),mzZ_p(i),ms0_p(i),ts_p(i),div_v_p(i)
     &  ,h_p(i),pn,idc_p(i,ip),felec_p(i),fHI_p(i),fHII_p(i)
     &  ,fHM_p(i),fH2I_p(i),fH2II_p(i),fHeI_p(i)
     &  ,fHeII_p(i),fHeIII_p(i),dvx_p(i),dvy_p(i),dvz_p(i)
     &  ,ndu_p(i)
 162        format(22(1pE13.5),2I10,13(1pE13.5))
          enddo
 70       close(50)
          ng(ip) = i
          write(6,*) totstep,'step rank=',ip,' ng=',ng(ip)
        enddo
c *** check for the overlap ***
        do i=0,ng(0)-1
          do j=0,ng(1)-1
            if(idc_p(i,0).eq.idc_p(j,1)) then
              write(6,*) i,idc_p(i,0),' in rank 0 also in rank 1 j=',j
              stop
            endif
          enddo
        enddo
      enddo

      stop
      end
