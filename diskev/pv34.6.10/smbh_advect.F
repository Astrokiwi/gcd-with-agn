#include "gcdp.def"

c Do black hole feedback
c Unfortunately, this needs to be f77 format to fit in with common blocks etc

#ifdef SMBH
#ifdef SMBH_ADVECT

      subroutine smbh_advect(np,ndm)
       use gcdp_smbh
       include './common.for'
       
       integer, intent(in) :: np,ndm
       
       integer :: ii,pn
     
       double precision :: mcbh_x,mcbh_y,mcbh_z,mcbh_m ! Centre of mass, for smbh_advect
       
       double precision :: dist2,hbh2
       
       double precision :: hdist,vdist,cdist,movedist
       double precision :: move_x,move_y,move_z
   
       !**** Find the local centre of mass of stuff within a certain radius around the SMBH
       ! Also do sums for centre of mass, if needed for SMBH advection

       mcbh_x = 0.
       mcbh_y = 0.
       mcbh_z = 0.
       mcbh_m = 0.
       
       ! SMBH is already synced, so we can all add mass locally
       ! Loop through *all* particles, not just active/gas/etc
       ! Then do an MPI_REDUCE!
       
       hbh2 = h_bh(0)**2 ! All particles within smoothing length
       ! Loop through baryons
       do ii=0,np-1
        dist2 = (x_p(ii)-x_bh(0))**2 +
     &      (y_p(ii)-y_bh(0))**2 +
     &      (z_p(ii)-z_bh(0))**2
        if ( dist2<=hbh2 ) then
         mcbh_x = mcbh_x + x_p(ii) * m_p(ii)
         mcbh_y = mcbh_y + y_p(ii) * m_p(ii)
         mcbh_z = mcbh_z + z_p(ii) * m_p(ii)
         mcbh_m = mcbh_m + m_p(ii)
        endif
       end do

! Dark matter counts too!
#ifdef DM
       do ii=0,ndm-1
        dist2 = (x_dm(ii)-x_bh(0))**2 +
     &      (y_dm(ii)-y_bh(0))**2 +
     &      (z_dm(ii)-z_bh(0))**2
        if ( dist2<=hbh2 ) then
         mcbh_x = mcbh_x + x_dm(ii) * m_dm(ii)
         mcbh_y = mcbh_y + y_dm(ii) * m_dm(ii)
         mcbh_z = mcbh_z + z_dm(ii) * m_dm(ii)
         mcbh_m = mcbh_m + m_dm(ii)
        endif
       end do
#endif
       
       ! pack into tvdr
       tdvs(0) = mcbh_x
       tdvs(1) = mcbh_y
       tdvs(2) = mcbh_z
       tdvs(3) = mcbh_m
       ! allreduce(sum)
       CALL MPI_ALLREDUCE(tdvs, tdvr, 4, MPI_DOUBLE_PRECISION,
     &        MPI_SUM, MPI_COMM_WORLD, ierr) 

       ! Get the CoM
       
       mcbh_x = tdvr(0)
       mcbh_y = tdvr(1)
       mcbh_z = tdvr(2)
       mcbh_m = tdvr(3)
       
       if ( mcbh_m<=0. ) then
        print *,"Lonely black hole error"
        print *,"Please rewind tape"
        stop
       endif

       mcbh_x = mcbh_x/mcbh_m
       mcbh_y = mcbh_y/mcbh_m
       mcbh_z = mcbh_z/mcbh_m

       ! then move the smbh accordionly!

       
       ! Only if I have the black hole!
       if ( myrank==iproc_bh(0) ) then
        pn = list_bh(0)
        
        
        ! Set the max distance to transpose
        
        hdist = 0.05 * sqrt(hbh2) ! 0.1 in WT12 because their smoothing length is half of ours (cutoff at 2 smoothing lengths instead of 1)
!        vdist = (vx_pbh(0)-vx_bh(0))**2 +
!     &          (vy_pbh(0)-vy_bh(0))**2 +
!     &          (vz_pbh(0)-vz_bh(0))**2
        vdist = (vx_pbh(0))**2 +
     &          (vy_pbh(0))**2 +
     &          (vz_pbh(0))**2
        vdist = 0.3*sqrt(vdist)*TM_dt
        cdist = (mcbh_x-x_p(pn))**2 +
     &          (mcbh_y-y_p(pn))**2 +
     &          (mcbh_z-z_p(pn))**2
        cdist = sqrt(cdist)
        
        movedist = min(hdist,vdist,cdist)
        
        move_x = (mcbh_x-x_p(pn)) * movedist!/cdist
        move_y = (mcbh_y-y_p(pn)) * movedist!/cdist
        move_z = (mcbh_z-z_p(pn)) * movedist!/cdist
        
        x_p(pn) = x_p(pn) + move_x
        y_p(pn) = y_p(pn) + move_y
        z_p(pn) = z_p(pn) + move_z

        write(*,"('SMBHCENT',6E14.5)") mcbh_x,mcbh_y,mcbh_z,move_x,
     &        move_y,move_z

        
!        print *,"SMBH ADVECT:",x_p(pn),xc_p(pn),mcbh_x
        
        ! Test version
C        x_p(pn) = x_p(pn) + bhspeed*TM_dt
C        y_p(pn) = y_p(pn) + bhspeed*TM_dt
C        z_p(pn) = z_p(pn) + bhspeed*TM_dt
c        x_p(pn) = mcbh_x
c        y_p(pn) = mcbh_y
c        z_p(pn) = mcbh_z
        
C        ! 1st order approx: teleport the SMBH
C        xc_p(pn) = x_p(pn)
C        yc_p(pn) = y_p(pn)
C        zc_p(pn) = z_p(pn)
       endif
       
      end subroutine smbh_advect

#endif
#endif
