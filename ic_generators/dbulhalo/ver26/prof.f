c /***************************************************
c   prof.f  for bulhalo
c  22 Aug. 2006  written by D.Kawata
c ****************************************************/
c70*******************************************************************

c /*****   NFW density profile ****/
      function rhonfwsw(r,c,rvir,rt,flag)
      include 'define.f'
      integer flag
      double precision rhonfwsw
      double precision r,c,rvir,rcut
      double precision x,t,a,n,ct,rt
 
      x=r/rvir
      t=rt*c/rvir

      if(flag.eq.0) then
        rhonfwsw=dexp(-r**2/rt**2)/(c*x*((1.0d0+c*x)**2))
      else
        rhonfwsw=1.0d0/(c*x*((1.0d0+c*x)**2))
      endif

      return
      end

      function mrnfw(r,c,rvir)
      include 'define.f'
      double precision mrnfw
      double precision r,c,rvir
 
      mrnfw=(rvir**3)*(4.0d0*M_PI)*((c*r+(c*r+rvir)*dlog(rvir))
     &   /(-c*r-rvir)+dlog(c*r+rvir))/(c**3)

      return
      end

c /*****   Hernquist mass profile  *****/
c /*****   M(r)  *****/
      function mrhp(r,ms,ah)
      include 'define.f'
      double precision mrhp
      double precision r,ms,ah

      mrhp = ms*(r**2)/((r+ah)**2)

      return
      end

c /*****   rho(r)  *****/
      function rhohp(r,ms,ah)
      include 'define.f'
      double precision rhohp
      double precision r,ms,ah

      rhohp = (ms/(2.0d0*M_PI))*(ah/r)/((r+ah)**3)

      return
      end

c /*****   r mrhp(r)  *****/
      function rmrhp(f,ms,ah)
      include 'define.f'
      double precision rmrhp
      double precision f,ms,ah

      rmrhp = ah*dsqrt(f)/(1.0d0-dsqrt(f))

      return
      end

c /*****   Exponential disk mass profile  *****/
c /*****   M(r)  from particle *****/
      function mr3dexpd(r,nd,rlistd,r3dd,massp)
      include 'define.f'
      double precision mr3dexpd
      integer i,ip,ipp
      integer nd,rlistd(MN)
      double precision r,r3dd(MN),mrp,massp(MN)

      mrp = 0.0d0
      ipp = rlistd(1)
      do i=1,nd
        ip=rlistd(i)
        if(r3dd(ip).lt.r) then
          mrp=mrp+massp(ip)
        else
          if(ipp.eq.rlistd(1)) then
            ip = rlistd(2)
            mrp=massp(ip)+massp(ip)*(r-r3dd(ipp))/(r3dd(ip)-r3dd(ipp))
          else
            mrp=mrp+massp(ip)*(r-r3dd(ipp))/(r3dd(ip)-r3dd(ipp))
          endif
          goto 70
         endif
         ipp = ip
      enddo
 70   mr3dexpd = mrp
      if(mr3dexpd.lt.0.0d0) then
        mr3dexpd = 0.0d0
      endif

      return
      end

c /*****   M(<r)  assuming thin disk *****/
      function mrexpdf(r,hd,mdisk)
      include 'define.f'
      double precision mrexpdf
      double precision r,hd,mdisk

c      if(flagdp.eq.0) then
        mrexpdf = mdisk*(1.0d0-(1.0d0+r/hd)*dexp(-r/hd))
c      else
c        mrexpdf = mdisk*(1.0d0-(1.0d0+(r/hd)+0.5d0*((r/hd)**2)
c     &    +(1.0d0/6.0d0)*((r/hd)**3))*dexp(-r/hd))
c      endif

      return
      end

c /*****   M(r)  for hot gas *****/
      function mrhgr(r,nmrhg,rhg,mrhg)
      include 'define.f'
      double precision mrhgr
      double precision r,rhg(MNR),mrhg(MNR)
      integer i,ir,nmrhg

      if(nmrhg.eq.0) then
        mrhgr=0.0d0
        return
      endif
      do i=1,nmrhg
        if(r.lt.rhg(i)) then
          ir=i
          goto 90
        endif
      enddo
c      write(6,*) ' for mrhg r,rhgmax=',r,rhg(nmrhg)
      ir=nmrhg
 90   mrhgr=mrhg(ir-1)+(r-rhg(ir-1))*(mrhg(ir)-mrhg(ir-1))
     &  /(rhg(ir)-rhg(ir-1))

      return
      end


c  ***** Mtot(<r) ***
      function mrtotf(r,nmrach,mrach,rmrach,mbul,ah
     &  ,ndisk,mdisk,hd,flagdp,nd,rlistd,r3dd,massp,nmrhg,rhg,mrhg)
      include 'define.f'
      double precision mrtotf
      integer nmrach
      integer id,ndisk,flagdp(MNC)
      double precision r,mrach(MNR),rmrach(MNR),mbul,ah
      double precision mdisk(MNC),hd(MNC)
      integer nd,rlistd(MN),nmrhg
      double precision r3dd(MN),massp(MN),mrhg(MNR),rhg(MNR)
      integer ir
      double precision mrh,mrb,mrd,mrhgr
      double precision mrhp,mrexpdf,mr3dexpd
      external mrhp,mrexpdf,mr3dexpd

c if there is no halo
      if(nmrach.eq.0) then
        mrh = 0.0d0
        goto 72
      endif
      do ir=1,nmrach
        if(rmrach(ir).gt.r) then
          goto 71
        endif
      enddo
      write(6,*) ' Warning: Cannot find r for Mr_ach in mrtotf()'
      write(6,*) ' r =',r,' set ir = nmrach r=',rmrach(nmrach)
      ir = nmrach
 71   if(ir.eq.1) then
        ir = 2
      endif
c *** halo mass ***
      mrh = 10.0d0**(dlog10(mrach(ir-1))
     &  +(dlog10(r)-dlog10(rmrach(ir-1)))
     &  *(dlog10(mrach(ir))-dlog10(mrach(ir-1)))
     &  /(dlog10(rmrach(ir))-dlog10(rmrach(ir-1))))
c *** bulge mass ***
 72   mrb = mrhp(r,mbul,ah)
c *** disk mass ***
      mrd=0.0d0
      do id=1,ndisk
         mrd=mrd+mrexpdf(r,hd(id),mdisk(id))
      enddo
c      mrd = mr3dexpd(r,nd,rlistd,r3dd,massp)
c *** halo gas mass ***
      mrhgr=0.0d0
      if(nmrhg.gt.0) then
        do ir=1,nmrhg
          if(rhg(ir).gt.r) then
            goto 73
          endif
        enddo
        write(6,*) ' Warning: Cannot find r for Mr_ach in mrtotf()'
        write(6,*) ' r =',r,' set ir = nmrhg'
        ir = nmrhg
 73     if(ir.eq.1) then
          ir = 2
        endif
        if(mrhg(ir).gt.0.0d0.and.mrhg(ir-1).gt.0.0d0) then
          mrhgr=10.0d0**(dlog10(mrhg(ir-1))
     &      +(dlog10(r)-dlog10(rhg(ir-1)))
     &      *(dlog10(mrhg(ir))-dlog10(mrhg(ir-1)))
     &      /(dlog10(rhg(ir))-dlog10(rhg(ir-1))))
        else 
          mrhgr=mrhg(ir-1)+(r-rhg(ir-1))
     &      *(mrhg(ir)-mrhg(ir-1))
     &      /(rhg(ir)-rhg(ir-1))
        endif
c      write(6,*) ' mrh,mrb,mrd,mrhg=',mrh,mrb,mrd,mrhgr
      else
        mrhgr=0.0d0
      endif
      mrtotf=mrh+mrb+mrd+mrhgr


      return
      end

      
