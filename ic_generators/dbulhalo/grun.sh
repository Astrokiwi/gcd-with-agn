#!/bin/bash
##PBS -l nodes=8:ppn=12
#PBS -l nodes=1:ppn=12
#PBS -j oe
#PBS -o dbulhalo.out
##PBS -l pmem=1700m
##PBS -l walltime=192:00:00
#PBS -l walltime=0:15:00
#PBS -N dbulhalo
#PBS -A dhg-391-ac
ulimit -a
cd $PBS_O_WORKDIR
#module add mvapich2/1.6-gcc gcc/4.8.2
#mpirun -np 96 ./pgen
module add gcc/4.8.2 openmpi/1.6.3-gcc
mpirun -np 12 ./pgen
