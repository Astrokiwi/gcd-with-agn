#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -j oe
#PBS -o dbulhalo.out
##PBS -l pmem=1700m
#PBS -l walltime=0:15:00
#PBS -N dbulhalo
#PBS -A dhg-391-ac
ulimit -a
cd $PBS_O_WORKDIR
mpiexec -n 8 ./pgen
