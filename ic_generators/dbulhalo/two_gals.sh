# bash script to 
if [ "$#" -ne 1 ]; then
    echo "./two_gals X"
    echo "Where X is the number of processors"
    exit 1
fi
echo "Running on $1 procs"
cp ini/input1.dat ini/input.dat
mpirun -np $1 ./pgen
./movedata.sh 1
cp ini/input2.dat ini/input.dat
mpirun -np $1 ./pgen
./movedata.sh 2
