echo "Copying to position $1"
cp output/diskc1.dat ../new_dbulhalo/ini/disk$1-1.dat
cp output/diskc2.dat ../new_dbulhalo/ini/disk$1-2.dat
cp output/bulge.dat ../new_dbulhalo/ini/bulge$1.dat
cp output/halo.dat ../new_dbulhalo/ini/halo$1.dat
